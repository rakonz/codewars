 public class Spiralizor {

    public static int[][] spiralize(int size) {
     
  if (size <= 0) {
    return null;
  };
  int[][] espiral = new int[size][size];
  int minimoColumna = 0;
  int maximoColumna = size-1;
  int minimoFila = 0;
  int maximoFila = size-1;
  
  for (int i = 0; i < espiral.length; i++) {
    for (int j = 0; j < espiral.length; j++) {
      espiral[i][j]=0;
    }
  }
  
  while (minimoFila <= maximoFila) {
    for (int i = minimoColumna; i <= maximoColumna; i++) {
      espiral[minimoFila][i] = 1;
    }
    for (int i = minimoFila; i <= maximoFila; i++) {
      espiral[i][maximoColumna] = 1;
    }
    
    if(minimoColumna != 0) {
      minimoColumna+=1;
    }
    if(maximoFila-1 == minimoFila) {
      break;
    }
    
    for (int i = maximoColumna-1; i >= minimoColumna; i--) {
      espiral[maximoFila][i] = 1;
    }
    
    for (int i = maximoFila; i >= minimoFila+2; i--) {
      espiral[i][minimoColumna] = 1;
    }
    
    minimoColumna+=1;
    minimoFila+=2;
    maximoColumna-=2;
    maximoFila-=2;
  }
  return espiral;
}
 

}


import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class SpiralizorTests {

    @Test
    public void test5() {
        assertArrayEquals(
                new int[][] {
                        { 1, 1, 1, 1, 1 },
                        { 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1 }
                },
                Spiralizor.spiralize(5));
    }

    @Test
    public void test8() {
        assertArrayEquals(
                new int[][] {
                        { 1, 1, 1, 1, 1, 1, 1, 1 },
                        { 0, 0, 0, 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 0, 1, 0, 1 },
                        { 1, 0, 1, 0, 0, 1, 0, 1 },
                        { 1, 0, 1, 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1, 1, 1, 1 },
                },
                Spiralizor.spiralize(8));
    }
    // Se evaúa la matriz dada de 9x9, por la que debería retornar el método spiralize con el size=9
  @Test
    public void test9() {
        assertArrayEquals(
                new int[][] {
                        { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                        { 0, 0, 0, 0, 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 0, 0, 1, 0, 1 },
                        { 1, 0, 1, 1, 1, 0, 1, 0, 1 },
                        { 1, 0, 1, 0, 0, 0, 1, 0, 1 },
                        { 1, 0, 1, 1, 1, 1, 1, 0, 1 },
                        { 1, 0, 0, 0, 0, 0, 0, 0, 1 },
                        { 1, 1, 1, 1, 1, 1, 1, 1, 1 },
                },
                Spiralizor.spiralize(9));
    }

}