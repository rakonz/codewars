public class RomanNumerals {
  private static int[] numerosActuales = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
  private static String[] numerosRomanos = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
  
  public static String toRoman(int n) {
   int numeroReal = n;
   StringBuilder resultado = new StringBuilder();
    for (int i = 0; i < numerosActuales.length; i++) {
       while (numeroReal >= numerosActuales[i]) {
                numeroReal -= numerosActuales[i];
                resultado.append(numerosRomanos[i]);
            }
        }

        return resultado.toString();
  }
  
  public static int fromRoman(String romanNumeral) {
     String numeroRomano = romanNumeral;
        int resultado = 0;

        for(int i = 0; i<numerosRomanos.length; i++) {
            while(numeroRomano.startsWith(numerosRomanos[i])) {
                numeroRomano = numeroRomano.substring(numerosRomanos[i].length(), numeroRomano.length());
                resultado += numerosActuales[i];
            }
        }
        return resultado;
  } 
}
 
//  Tests
 import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RomanNumeralsTest {

    @Test
    public void testToRoman() throws Exception {
        assertThat("1 converts to 'I'", RomanNumerals.toRoman(1), is("I"));
        assertThat("2 converts to 'II'", RomanNumerals.toRoman(2), is("II"));
      
//       Se evalúa la expresión RomanNumerals.toRoman(1009) para que se compare con 'MIX';
        assertThat("1009 debe convertirse a 'MIX'", RomanNumerals.toRoman(1009), is("MIX"));
    }

    @Test
    public void testFromRoman() throws Exception {
        assertThat("'I' converts to 1", RomanNumerals.fromRoman("I"), is(1));
        assertThat("'II' converts to 2", RomanNumerals.fromRoman("II"), is(2));
    }
}