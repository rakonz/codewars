export class Challenge {
    static solution(number: number) {
  //     Se crea variable inicializada en 0
     var sumaNumeros = 0;
  //     Se recorre el largo del número ingresado
      for (let i = 0; i < number; i++) {
  //       Se filtra, si el resto divido en 5 es 0 y el resto dividido en 3 es 0.
        if(i % 5==0 || i % 3==0 ) {
  //         Se suma a la variable anterior, la suma de los números con resto 0
          sumaNumeros+=i;  
          continue;
       } else {
          continue;
        }
      }
      return sumaNumeros;
    }
    } 

    // Tests
import solution = require('./solution');
import {assert} from "chai";

function test(num: number, expected: number) {
  assert.strictEqual(solution.Challenge.solution(num), expected)
}

describe("solution", function(){
  it("should handle basic tests", function() {
    test(10, 23);
  });
// Valores sumados hasta el número 5, que sean divisibles por 5 o 3.
   it("testt", function() {
    test(6, 8);
  });
});