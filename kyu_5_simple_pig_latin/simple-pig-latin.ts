export const pigIt = (a : string) : string =>  {
  
    //   Se hace un split del string ingresado
    let palabraArray = a.split(' ');
    // Se crea regex con letras del abecedario
    let regex = new RegExp('([a-zA-Z])');
    //   Se inicaliza variable string palabra
    let palabra!: string;
    //   Se inicializa una lista vacía que contendrá la palabra final
    let palabraFinal = [];
    
    
      for ( palabra of palabraArray) { 
    //    Se evalúa si el regex contiene la palabra
        if ((regex).test(palabra)) {
    //       En caso de que cumpla la condición, se añade la palabra a la lista vacía 'palabraFinal', se cambia el orden de la palabra
    //       moviendo la posición 0 que sería la primera letra de la palabra, al lado derecho y sumándole el array 'ay'
          palabraFinal.push(palabra.substring(1) + palabra[0] + "ay"); 
        } else { 
    //       En caso contrario, se suma la palabra a la lista palabraFinal
          palabraFinal.push(palabra); 
        } 
      }
      palabra.substring(1) + palabra[0] + "ay"
      return palabraFinal.join(" ");
    }

    // Tests
    // See https://www.chaijs.com for how to use Chai.
import { assert } from "chai";

import { pigIt } from "./solution";

describe("Tests", () => {
  it("test", () => {
    assert.strictEqual(pigIt('Pig latin is cool'),'igPay atinlay siay oolcay')
    assert.strictEqual(pigIt('This is my string'),'hisTay siay ymay tringsay')
    
    assert.strictEqual(pigIt('Esto es un test'),'stoEay seay nuay esttay')
});
});
