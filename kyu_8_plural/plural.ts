export function plural(n:number):boolean {
    // Si el número ingresado es distinto a uno, retorna true
    if(n!=1) {
      return true;
    }
    // En caso contrario, retorna false 
    return false;
  }

//   Tests
import solution = require('./solution');

import {assert} from "chai";

describe("solution", function(){
  it("BasicTests", function(){
    assert.equal(solution.plural(0), true, "Plural for 0");
    assert.equal(solution.plural(0.5), true, "Plural for 0.5");
    assert.equal(solution.plural(1), false, "Plural for 1");
    assert.equal(solution.plural(100), true, "Plural for 100");
    assert.equal(solution.plural(Infinity), true, "Plural for Infinity");
//     Test
    assert.equal(solution.plural(5), true, "Plural for 5");
  });
});