public class Fibonacci {
	public static long fib (int n) {
//    fn = fn-1 + fn-2
		if (n==1) {
      return n;
    } else if (n==0) {
      return n;
    }
    return fib(n-1) + fib(n-2);
  }
		
	}
 
//  Tests
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

// TODO: Replace examples and use TDD development by writing your own tests

class SolutionTest {
    @Test
    void sampleTests() {
        assertEquals(1, Fibonacci.fib(1), "fib(1)");
        assertEquals(1, Fibonacci.fib(2), "fib(2)");
        assertEquals(2, Fibonacci.fib(3), "fib(3)");
        assertEquals(3, Fibonacci.fib(4), "fib(4)");
        assertEquals(5, Fibonacci.fib(5), "fib(5)");
//       Evaluar si fibonacci f10 = 55
        assertEquals(55, Fibonacci.fib(10), "fib(10)");
    }
}